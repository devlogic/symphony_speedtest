import speedtest
import requests
import json
import os
import urllib3


def do_speedtest():
    url = os.environ['SYMPHONY_URL'] if 'SYMPHONY_URL' in os.environ else 'https://hr.symphony.is'
    email = os.environ['SYMPHONY_EMAIL'] if 'SYMPHONY_EMAIL' in os.environ else None
    token = os.environ['SYMPHONY_TOKEN'] if 'SYMPHONY_TOKEN' in os.environ else None

    if not token or not email:
        print("Email and token must be set in the environment variables (SYMPHONY_EMAIL and SYMPHONY_TOKEN) \n Email is: first_name.last_name@symphony.is \n Token can be retrieved via Symphony TimeTracker Bot by simply typing '/token' anywhere in slack")
        return False

    try:
        s = speedtest.Speedtest()
        # s.get_servers(servers)
        s.get_best_server()
        s.download()
        s.upload()
        s.results.share()
        results_dict = s.results.dict()

        download = results_dict['download']
        upload = results_dict['upload']
        ping = results_dict['ping']
    except:
        print("Speedtest failed to execute")
        return False

    try:
        data = { 'email': email, 'token':token, 'download': download, 'upload': upload, 'ping': ping, 'raw': json.dumps(results_dict), }
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        response = requests.post(url + '/api/speedtest', data=data, verify=False, )
        response.raise_for_status()
    except requests.exceptions.RequestException as err:
        print ("Http error happened while trying to invoke Symphony API: ",err)
        return False
    except requests.exceptions.HTTPError as e:
        print (e.response.text)
        return False
    except:
        print("Symphony API failed to execute")
        return False

    return True