import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="symphony-speedtest",
    version="0.0.3",
    author="Dino Osmanovic",
    author_email="dino.osmanovic@symphony.is",
    description="Package that allows Speedtest to be used within the organization",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/devlogic/symphony_speedtest",
    packages=['symphony_speedtest', ],
    install_requires=['speedtest-cli', 'requests', ],
    classifiers=[
        "Programming Language :: Python :: 2",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # python_requires='>=3.6',
    zip_safe=False,
    entry_points = {
        'console_scripts': ['symphony-speedtest=symphony_speedtest.command_line:main'],
    }
)